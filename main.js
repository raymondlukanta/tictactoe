'use strict';

const readline = require('readline');
const Promise = require('bluebird');
const helper = require('./helper');
const configs = require('./configs');
const constants = require('./constants');
const humanGame = require('./core/humanGame');
const robotGame = require('./core/robotGame');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

helper.promiseWhile((chosenMenu) => {
    return parseInt(chosenMenu) !== 3;
}, function getMenuInput() {
    return new Promise((resolve) => {
        printGameInstruction();
        rl.question('Choose Menu: ', (chosenMenu) => { resolve(chosenMenu) })
    })
    .then((chosenMenu) => {

        switch(parseInt(chosenMenu)) {
            case 1:
                process.stdout.write('\x1Bc');
                return humanGame.startGameWithHuman(rl);
            case 2:
                process.stdout.write('\x1Bc');
                return robotGame.startGameWithBot(rl);
            case 3:
                process.stdout.write('\x1Bc');
                return 3;
            default:
                process.stdout.write('\x1Bc');
                console.log('Wrong input\n\n');
                return 0;
        }

    })
    .catch((err) => {
        console.log(err)
    });

}, 0)
.then(() => {
    console.log('Quit game. Good bye!');
    process.exit();
});

function printGameInstruction() {
    console.log('Tic-tac-toe');
    console.log('----------');
    console.log('1. Play w/ Human');
    console.log('2. Play w/ Bot');
    console.log('3. Exit\n');
};
