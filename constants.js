function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true,
    });
}

// define constants here
define('NO_WINNER', 0);
define('PLAYER_1_WIN', 1);
define('PLAYER_2_WIN', 2);
define('DRAW', 3);
define('EMPTY_CELL', '.');
define('PLAYER_1_CHARACTER', 'X');
define('PLAYER_2_CHARACTER', 'O');
