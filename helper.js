'use strict';

const Promise = require('bluebird');

var Helper = function() {

    var helper = this;

    this.promiseWhile = Promise.method(function promiseWhile(condition, action, value) {
        if (!condition(value)) {
            return value;
        }

        return action(value).then(helper.promiseWhile.bind(null, condition, action));
    });

};

module.exports = new Helper();
