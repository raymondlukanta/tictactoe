function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true,
    });
}

// define configs here
define('BOARD_SIZE', 3);
