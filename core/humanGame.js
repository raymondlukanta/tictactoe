'use strict';

const Promise = require('bluebird');
const configs = require('../configs');
const helper = require('../helper');
const commonGame = require('./commonGame');

var HumanGame = function() {

    this.startGameWithHuman = function startGameWithHuman(rl) {

        var initialGameObject = {
            winner: 0,
            step: 0,
            player: 1,
            board: commonGame.initializeBoard(configs.BOARD_SIZE),
        }

        return Promise.try(() => {
            return commonGame.printBoard(initialGameObject)
        })
        .then(() => {
            return getPlayerMovement(initialGameObject, rl);
        });

    };

    function getPlayerMovement(initialGameObject, rl) {

        return helper.promiseWhile((gameObject) => {
            return gameObject.winner == false;
        }, function getMovement(gameObject) {

            return commonGame.getHumanMovement(gameObject, rl);

        }, initialGameObject)
        .then((gameObject) => {

            return commonGame.printResultPlayer(gameObject, rl);

        });

    };

};

module.exports = new HumanGame();
