'use strict';

const Promise = require('bluebird');
const configs = require('../configs');
const constants = require('../constants');
const helper = require('../helper');
const commonGame = require('./commonGame');

var RobotGame = function RobotGame() {

    this.startGameWithBot = function startGameWithBot(rl) {

        var initialGameObject = {
            winner: 0,
            step: 0,
            player: 1,
        }

        return new Promise((resolve) => {
            rl.question(
                'You choose the first player (X) or second player (O) [X/O]? ',
                (chosenPlayer) => { resolve(chosenPlayer) }
            );
        })
        .then((chosenPlayer) => {

            switch (chosenPlayer) {
                case constants.PLAYER_1_CHARACTER:
                    initialGameObject.humanPlayer = 1;
                    break;
                case constants.PLAYER_2_CHARACTER:
                    initialGameObject.humanPlayer = 2;
                    break;
            }

            initialGameObject.board = commonGame.initializeBoard(configs.BOARD_SIZE);

            return Promise.try(() => {
                return commonGame.printBoard(initialGameObject)
            })
            .then(() => {

                return getPlayerMovement(initialGameObject, rl);
            });
        });

    }

    function getPlayerMovement(initialGameObject, rl) {

        return helper.promiseWhile((gameObject) => {
            return gameObject.winner == false;
        }, function getMovement(gameObject) {
            if (gameObject.player === gameObject.humanPlayer) {

                return commonGame.getHumanMovement(gameObject, rl);

            } else {

                return calculateBotMovement(gameObject);
            }

        }, initialGameObject)
        .then((gameObject) => {
            return commonGame.printResultPlayer(gameObject, rl);
        })

    }

    function calculateBotMovement(gameObject) {

        return findBestMove(gameObject)
        .then((bestMove) => {

            var row = bestMove.row;
            var col = bestMove.col;

            gameObject.board = commonGame.move(row, col, gameObject);
            gameObject.step++;
            gameObject.winner = commonGame.getWinner(row, col, gameObject);
            gameObject.player = (gameObject.step % 2) + 1;

            console.log('Bot is moving...\n\n')
            switch (gameObject.winner) {
                case constants.PLAYER_1_WIN:
                case constants.PLAYER_2_WIN:
                case constants.DRAW:
                    return gameObject;
                case constants.NO_WINNER:
                    commonGame.printBoard(gameObject);
                    return gameObject;


            }
        });

    }

    function isMovesLeft(board) {
        for (var r = 0; r < board.length; r++) {
            for (var c = 0; c < board.length; c++) {

                if (board[r][c] === constants.EMPTY_CELL) {
                    return true;
                }

            }
        }

        return false;
    }

    function miniMax(row, col, gameObject, isMax) {
        var player = gameObject.player;
        var winner = commonGame.getWinner(row, col, gameObject);
        var character = commonGame.getCharacterFromPlayer(player);

        switch(winner) {
            case gameObject.humanPlayer:
                return -10 + gameObject.step;
                break;
            case constants.DRAW:
            case constants.NO_WINNER:
                return 0;
                break;
            default:
                return 10 - gameObject.step;
                break;
        }

        if (!isMovesLeft(gameObject.board)) {
            return 0;
        }

        var best;
        if (isMax) {
            best = -1000;
        } else {
            best = 1000;
        }

        for (var r = 0; r < gameObject.board.length; r++) {
            for (var c = 0; c < gameObject.board.length; c++) {

                if (gameObject.board[r][c] === constants.EMPTY_CELL) {

                    gameObject.board[r][c] = character;

                    gameObject.step++;
                    gameObject.player = (gameObject.step % 2) + 1;

                    // Call miniMax recursively and choose the maximum/ minimum value
                    if (isMax) {
                        best = Math.max(best, miniMax(r, c, gameObject, !isMax));
                    } else {
                        best = Math.min(best, miniMax(r, c, gameObject, !isMax));
                    }

                    // Undo the move
                    gameObject.board[r][c] = constants.EMPTY_CELL;
                }

            }
        }
        return best;

    }

    function findBestMove(gameObject) {

        var character = commonGame.getCharacterFromPlayer(gameObject.player);

        var bestVal = -1000;
        var bestMove = {
            row: -1,
            col: -1,
        }

        for (var r = 0; r < gameObject.board.length; r++) {
            for (var c = 0; c < gameObject.board.length; c++) {

                if (gameObject.board[r][c] === constants.EMPTY_CELL) {

                    gameObject.board[r][c] = character;

                    // compute evaluation function for this move.
                    var moveVal = miniMax(r, c, gameObject, false);

                    // Undo the move
                    gameObject.board[r][c] = constants.EMPTY_CELL;

                    // Find the best move
                    if (moveVal > bestVal)
                    {
                        bestMove.row = r;
                        bestMove.col = c;
                        bestVal = moveVal;
                    }
                }

            }
        }

        return Promise.resolve(bestMove);

    }
};

module.exports = new RobotGame();
