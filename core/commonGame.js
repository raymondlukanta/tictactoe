'use strict';

const Promise = require('bluebird');
const constants = require('../constants');
const helper = require('../helper');

var CommonGame = function CommonGame() {

    var commonGame = this;

    this.initializeBoard = function initializeBoard(n) {
        var board = [];
        for (var r = 0; r < n; r++) {
            board[r] = [];

            for (var c = 0; c < n; c++) {
                board[r][c] = constants.EMPTY_CELL;
            }
        }

        return board;

    };

    this.getHumanMovement = function getHumanMovement(gameObject, rl) {

        var userMovement;

        return helper.promiseWhile((isInputValid) => {
            return isInputValid === false;
        }, function getInput() {
            return new Promise((resolve) => {
                rl.question(`Player ${gameObject.player} move (row col): `, (rowCol) => {
                    resolve(rowCol)
                });
            })
            .then((rowCol) => {

                console.log('\n');
                var trimmedRowCol = rowCol.trim();
                if (!validateHumanMovementInput(trimmedRowCol)) {
                    console.log('Your input is not valid. Please try again.');
                    return false;

                } else {
                    userMovement = trimmedRowCol;
                    return true;
                }

            });
        }, false)
        .then(() => {
            var splittedUserMovement = userMovement.split(' ');

            var row = splittedUserMovement[0];
            var maskedRow = row - 1;

            var col = splittedUserMovement[1];
            var maskedCol = col - 1;

            if (isCellEmpty(maskedRow, maskedCol, gameObject.board)) {

                gameObject.board = commonGame.move(maskedRow, maskedCol, gameObject);
                gameObject.step++;
                gameObject.winner = commonGame.getWinner(maskedRow, maskedCol, gameObject);
                gameObject.player = (gameObject.step % 2) + 1;

                switch (gameObject.winner) {
                    case constants.PLAYER_1_WIN:
                    case constants.PLAYER_2_WIN:
                    case constants.DRAW:
                        return gameObject;
                    case constants.NO_WINNER:
                        commonGame.printBoard(gameObject);
                        return gameObject;
                }
            } else {
                console.log(`The cell (${row}, ${col}) is not empty. Please try again.`)
                return gameObject;
            }

        });

    }

    function validateHumanMovementInput(playerInput) {
        var regexNumber = /^\d \d$/;
        return regexNumber.test(playerInput);
    }

    function isCellEmpty(row, col, board) {
        return board[row][col] === constants.EMPTY_CELL;
    }


    this.printBoard = function printBoard(gameObject) {

        var n = gameObject.board.length;

        if (!gameObject.winner) {
            var message = `Turn #${gameObject.step+1}`;

            if (gameObject.humanPlayer) {
                if (gameObject.humanPlayer === gameObject.player) {
                    message += ' (You)';
                } else {
                    message += ' (Bot)';
                }
            }

            console.log(message);
        }

        var frame = '';
        for (var i = 0; i < ((n * 2) + 4); i++) {
            frame += '-';
        }

        process.stdout.write(`${frame}\n  | `);

        for (var i = 1; i <= n; i++) {
            process.stdout.write(`${i} `);
        }

        process.stdout.write('\n');

        process.stdout.write(`${frame}\n`);

        for (var r = 0; r < n; r++) {
            process.stdout.write(`${r+1} | `);

            for (var c = 0; c < n; c++) {

                process.stdout.write(`${gameObject.board[r][c]} `);

            }
            process.stdout.write('\n');

        }

        process.stdout.write('\n');

    };

    this.printResultPlayer = function printResultPlayer(gameObject, rl) {

        var winner = gameObject.winner;
        var message;
        var acclamations = '!!!! !!!! !!!! !!!! !!!!';

        if (winner === constants.DRAW) {
            message = 'Draw game';
        } else if (gameObject.humanPlayer) {

            switch (winner) {
                case gameObject.humanPlayer:
                    message = 'You win to the bot :)';
                    break;
                default:
                    message = 'You lose to the bot :(';
                    break;
            }

        } else {
            message = `The winner is player ${winner}`;
        }

        console.log(acclamations);
        console.log(message);
        console.log(acclamations);
        console.log();
        console.log('Last board:');
        commonGame.printBoard(gameObject);
        return new Promise((resolve) => {
            return rl.question(`Enter to continue...`, (rowCol) => {
                process.stdout.write('\x1Bc');

                return resolve(rowCol)
            });
        })

    };

    this.move = function move(row, col, gameObject) {

        var character = commonGame.getCharacterFromPlayer(gameObject.player);
        gameObject.board[row][col] = character;
        return gameObject.board;

    }

    this.getWinner = function getWinner(row, col, gameObject) {

        var n = gameObject.board.length;
        var character = commonGame.getCharacterFromPlayer(gameObject.player);


        //check row
        for (var i = 0; i < n; i++) {
            if (gameObject.board[row][i] !== character) {
                break;
            }

            if (i === n-1) {
                return gameObject.player;
            }
        }

        //check col
        for (var i = 0; i < n; i++) {
            if (gameObject.board[i][col] !== character) {
                break;
            }

            if (i === n-1) {
                return gameObject.player;
            }
        }

        //check diag
        if (row === col) {
            //we're on a diagonal
            for (var i = 0; i < n; i++) {
                if (gameObject.board[i][i] !== character) {
                    break;
                }

                if (i === n-1) {
                    return gameObject.player;
                }
            }
        }

        //check anti diagonal
        if (row + col === n - 1) {
            for (var i = 0; i < n; i++) {
                if (gameObject.board[i][(n-1) - i] !== character) {
                    break;
                }

                if (i === n-1) {
                    return gameObject.player;
                }
            }
        }

        //check draw
        if (gameObject.step === (n*n)) {
            return constants.DRAW;
        }

        return constants.NO_WINNER;
    }

    this.getCharacterFromPlayer = function getCharacterFromPlayer(player) {
        switch (player) {
            case 1:
                return constants.PLAYER_1_CHARACTER;
            case 2:
                return constants.PLAYER_2_CHARACTER;
        }
    }

};

module.exports = new CommonGame();
